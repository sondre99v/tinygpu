# TinyGPU

[Git repository](https://gitlab.com/sondre99v/TinyGPU)

## Idea
The idea of this project is to see if it is possible to generate a VGA video signal from the ATTiny817, and as few external components as possible.
The MCU would, of course, have to generate the pixel-data and synchronization signals. However, since memory is limited, a full frame-buffer is propably not feasable, so we need some kind of representation of what should be shown on the screen. As I imagine this could be useful to create simple games or interfaces, I decided to take inspiration from the NES-console (and the many variations on it's graphics system), and implement a tiles-and-sprites-based system.


### Pixel Speed
The first problem is that VGA signals require strict timing. Looking at http://tinyvga.com/vga-timing we can see that there are several options, but none of them have a "pixel clock" slower than the maximum clock speed allowed for the Tiny1-series of microcontrollers.

The "pixel clock" refers to the number of pixels emitted per second when the scanline is racing across the visible portion of the screen. If we group several pixels together, we can get away with a slower pixel clock, at the expense of horizontal resolution. With this in mind I settled on the 800x600@60Hz resolution, mostly because it has a pixel clock of 40MHz, which divides evenly into the 20MHz maximum clock frequency, and otherwise gives nice round numbers.

However, at 20MHz, the fastest one could hope to emit pixels if the CPU would be in control, would be through the below loop:
```
loop:
  ld r16, X+          ; 2 cycles
  out VPORTB_OUT, r16 ; 1 cycle
  dec r17             ; 1 cycle
  brne loop           ; 2 cycles
```

This is a 6-cycle loop, wich means that the pixel clock will have to be divided by 12 (since two "real" pixels are emitted every CPU clock cycle), giving an effective horizontal resolution of only 66 and two-thirds pixels. It also eats every single CPU-cycle during the visible portion of the screen, leaving very little time for any other processing, such as the required decoding to translate tile-indexes and sprite data into pixels.

## USART to the rescue
However, the power in microcontrollers are their peripherals. If we use the USART peripheral to stream bits out on the USART.TX pin, we can obtain much better results. The maximum USART clock is half of the CPU-clock, which gives us a pixel clock of 10MHz, resulting in a horizontal resolution of 200 pixels. Not great, but passable, and propably the best we can do.

Note also, that this only results in monochrome pixels, since the USART can only output a single bit at a time, and we only have a single USART peripheral to use. If we had multiple USARTs we could use one for each bit in whatever color-depth we wanted to implement, and then mix them together with external resistors. With a single USART, however, the best we can do is to emit the same signal to all three VGA color channels, giving a monochrome black-or-white image.

